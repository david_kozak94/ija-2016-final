/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >   
 *  
 *  Popis rozhrani GameMoveLogic :
 *  Rozhrani, ktere reprezentuje logiku tahu.
 */

package src.ija.ija2016.othello.board;

import java.util.List;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se mechanismu vkladani hracich kamenu
 */
public interface GameMoveLogic {
    /**
     * Metoda overi, zda je na danou pozici moznost vlozit kamen
     * @param dirs - smer, jakym z daneho pole ma prohledavani jit dal
     * @return uspech / neuspech
     */
    boolean checkMove(Field.Direction dirs,boolean isWhite, List<Field> stonesToChange);

    /**
     * Metoda obsahujici cinnost vykonanou pred vyvolanim checkMove nad dalsim objektem v rade
     */
    void preTest();

    /**
     * Metoda obsahujici cinnost vyvolanou po skonceni metody checkMove nad dalsim objektem v rade
     */
    void postTest();
}
