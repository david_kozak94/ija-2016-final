
/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *	Datum:      23 Apr 2016
 *	Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 * 	            Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >		
 *	
 *  Popis rozhrani AbstractField:
 *	Abstraktni trida reprezentujici obecne herni pole,
 *	obsahuje metody pro praci s hernim polem pro lubovolnou deskovou hru.
 */

package src.ija.ija2016.othello.board;

import java.util.HashMap;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 *  Abstraktni trida reprezentujici obecne herni pole
 */

public abstract class AbstractField implements Field {
    protected final int row, col;
    private HashMap<Direction, Field> neighbours = new HashMap<>();
    protected Disk disk = null;
    
    /**
     * Konstruktor tridi AbstractField
     * @param row pocet radku
     * @param col pocet stlpcu
     */
    public AbstractField(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * Metoda prida sousedni pole field v danom smere dirs
     * @param dirs smer
     * @param field pole
     */
    @Override
    public void addNextField(Direction dirs, Field field) {
        neighbours.put(dirs, field);
    }

    /**
     * Vrati sousedni pole v danem smere dirs
     * @param dirs smer
     * @return sousedni pole
     */
    @Override
    public Field nextField(Direction dirs) {
        return neighbours.get(dirs);
    }

    /**
     * Vraci kamen, ktery je vlozen na pole.
     * @return disk kamen
     */
    @Override
    public Disk getDisk() {
        return disk;
    }

    /**
     * Metoda vraci ci je kamen prazdny
     * @return true ak je prazdny
     */
    @Override
    public boolean isEmpty() {
        return disk == null;
    }


    /**
     * Metoda odstrani kamen
     */
    @Override
    public void removeDisk() {
        this.disk = null;
    }

    /**
     * Metoda vraci true v pripade rovnosti objektu
     * @param o objekt
     * @return true v pripade uspechu
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractField that = (AbstractField) o;
        return row == that.row && col == that.col;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + col;
        return result;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return isEmpty() ? " " : getDisk().isWhite() ? "W" : "B";
    }

    /**
     * Metoda getRow
     * @return row pocet radku
     */
    public int getRow() {
        return row;
    }

    /**
     * Metoda getRow
     * @return col pocet stlpcu
     */
    public int getCol() {
        return col;
    }
}
