/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >      
 *  
 *  Popis tridi Disk :
 *  Trida, ktera reprezentuje kamen hraci desky.
 */

package src.ija.ija2016.othello.board;

import java.io.Serializable;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida, ktera reprezentuje kamen hraci desky.
 */

public class Disk implements Serializable{
    private boolean isWhite;

    /**
     * Konstruktor pro farbu kamene
     * @param isWhite true- biely kamen, false- cerny kamen
     */
    public Disk(boolean isWhite) {
        this.isWhite = isWhite;
    }
    
    /**
     * Overeni, akej farby je kamen
     * @return true- biely kamen
     * @return false- cerny kamen
     */
    public boolean isWhite() {
        return isWhite;
    }
    /**
     * Metoda turn
     * zmeni farbu kamene z bile na cernou a z cerne na bilou
     */
    public void turn() {
        this.isWhite = !this.isWhite;
    }
}
