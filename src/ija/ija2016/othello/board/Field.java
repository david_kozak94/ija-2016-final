/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >       
 *  
 *  Popis rozhrani Field :
 *  Rozhrani k polim, ktore lze umistnit na hraci desku.
 */

package src.ija.ija2016.othello.board;

import java.io.Serializable;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se hraciho pole a kamenu hraci desky
 */

public interface Field extends GameMoveLogic,Serializable {
    /**
     * Vyctovy typ reprezentujici smery
     */
    enum Direction {
        D, L, LD, LU, R, RD, RU, U
    }

    void addNextField(Direction dirs,Field field);
    boolean canPutDisk(Disk disk);
    Disk getDisk();
    boolean isEmpty();
    Field nextField(Direction dirs);
    boolean putDisk(Disk disk);
    void removeDisk();
}
