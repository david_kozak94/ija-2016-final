/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >   
 *  
 *  Popis rozhrani Rules :
 *  Rozhrani, ktere reprezentuje pravidla inicializace hry.
 */

package src.ija.ija2016.othello.board;

import java.io.Serializable;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se pole a disku
 */

public interface Rules extends Serializable{

    Field createField(int row, int col);
    int getSize();
    int numberDisks();
}