/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >      
 *  
 *  Popis tridi BorderdField :
 *  Trida, ktera reprezentuje neaktivni pole hraci desky.
 */

package src.ija.ija2016.othello.board;

import java.util.List;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida, ktera reprezentuje neaktivni pole hraci desky
 */

public class BorderField implements Field {

    @Override
    public void addNextField(Direction dirs, Field field) {

    }

    @Override
    public boolean canPutDisk(Disk disk) {
        return false;
    }

    @Override
    public Disk getDisk() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public Field nextField(Direction dirs) {
        return null;
    }

    @Override
    public boolean putDisk(Disk disk) {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public boolean checkMove(Direction dirs, boolean isWhite, List<Field> stonesToChange) {
        stonesToChange.clear();
        return false;
    }

    @Override
    public void removeDisk() {

    }

    @Override
    public void preTest() {
    }

    @Override
    public void postTest() {
    }
}
