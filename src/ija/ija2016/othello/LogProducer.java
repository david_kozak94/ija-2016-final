/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >              
 *  
 *  Trida LogProceducer loguje informace
 */

package src.ija.ija2016.othello;
/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida LogProceducer loguje informace
 */

public class LogProducer {
    /**
     * Metoda pro ziskani informaci
     * @param l
     * @param msg
     */
    public static void info(Loggable l,String msg) {
        System.out.println("INFO class=" + l.getTag() + " msg=" + msg);
    }

    /**
     * Rozhrani tridy LogProcucer
     *
     */
    public interface Loggable{
        String getTag();
    }
}
