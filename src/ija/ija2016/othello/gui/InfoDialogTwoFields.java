/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >
 *
 *  Popis tridi InfoDialog :
 *  Pomocna trida pro vypis informaci ohledne autoru a instrukcii
 */

package src.ija.ija2016.othello.gui;

import javax.swing.*;
/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 *  Popis tridi InfoDialogTwoFields :
 *  Pomocna trida pro vypis informaci ohledne autoru a instrukcii
 */

public class InfoDialogTwoFields extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField infoText;
    private JTextField infoText2;

    private InfoDialogTwoFields(String title, String info, String info2) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        this.setTitle(title);
        this.infoText.setText(info);
        this.infoText2.setText(info2);
        this.setSize(500,200);
        this.setLocationRelativeTo(null);
        buttonOK.addActionListener(x->dispose());
    }

    /**
     * Metoda pro vypis informaci
     * @param title nadpis
     * @param info prvni radek textu
     * @param info2 druhy radek textu
     * @return dialog
     */
    public static InfoDialogTwoFields show(String title, String info, String info2) {
        InfoDialogTwoFields infoDialogTwoFields = new InfoDialogTwoFields(title, info, info2);
        SwingUtilities.invokeLater(()-> infoDialogTwoFields.setVisible(true));
        return infoDialogTwoFields;
    }
}
