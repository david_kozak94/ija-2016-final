/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >               
 *  
 *  Popis tridi LoadGameDialog :
 *  Dialog pro zobrazeni a znovuspusteni ulozene hry.
 */

package src.ija.ija2016.othello.gui;

import src.ija.ija2016.othello.IOmanager;
import src.ija.ija2016.othello.game.Game;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Dialog pro zobrazeni a znovuspusteni ulozene hry.
 */

public class LoadGameDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel mainPanel;

    /**
     * Metoda reprezentujici dialog nacitani hry
     * @param savedGames
     */
    public LoadGameDialog(List<String> savedGames) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        ButtonGroup buttonGroup = new ButtonGroup();
        savedGames.stream().
                map(x->x.replace(IOmanager.FILE_EXTENSION,"")).
                map(JRadioButton::new).
                forEach(
                        x -> {
                            buttonGroup.add(x);
                            mainPanel.add(x);
                        });


        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     * Metoda reprezentuje tlacitko OK
     */
    private void onOK() {
        List<String> l = Arrays.stream(mainPanel.getComponents()).map(x->(JRadioButton)x).filter(JRadioButton::isSelected).map(JRadioButton::getText).collect(toList());
        assert l.size() == 1;
        try {
            Game game = IOmanager.loadGame(l.get(0));
            new Thread(()->{
                MainWindow.reloadGame(game);
            }).start();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            dispose();
        }
    }

    /**
     * Metoda reprezentuje tlacitko Cancel
     */
    private void onCancel() {

        dispose();
    }

    /**
     * Metoda pro vytvoreni komponentu UI
     */
    private void createUIComponents() {
        mainPanel = new JPanel();
    }
}
