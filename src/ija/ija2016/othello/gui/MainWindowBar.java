/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >             
 *  
 *  Popis tridi MainWindowBar :
 *  Horni lista obsahujici prikazy pro nacteni, ulozeni, ukonceni hry a podobne.
 */

package src.ija.ija2016.othello.gui;

import src.ija.ija2016.othello.IOmanager;
import src.ija.ija2016.othello.game.Game;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Horni lista obsahujici prikazy pro nacteni, ulozeni, ukonceni hry a podobne.
 */

public class MainWindowBar extends JMenuBar {

    /**
     * Konstruktor tridi MainWindowBar
     * @param game hra
     */
    public MainWindowBar(Game game) {
        JMenu menu = new JMenu("Games");
        menu.setMnemonic(KeyEvent.VK_N);
        this.add(menu);

        JMenuItem item = new JMenuItem("New game");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener(x->new NewGameDialog());
        menu.add(item);

        item = new JMenuItem("Save game");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener(x -> {
            try {
                IOmanager.saveGame(game);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
        //item.addActionListener(new SaveGameListener());
        menu.add(item);

        item = new JMenuItem("Load game");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener(x->{
            List<String> savedGames = IOmanager.getSavedGames();
            if(!savedGames.isEmpty()){
                new LoadGameDialog(savedGames);
            } else{
                GenericDialog genericDialog = GenericDialog.show("Info","No saved games found");
                SwingUtilities.invokeLater(()->genericDialog.setSize(400,150));
            }
        });
        //item.addActionListener(new LoadGameListener());
        menu.add(item);

        item = new JMenuItem("Exit");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener((x)-> System.exit(0));
        menu.add(item);


        menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_N);
        this.add(menu);

        item = new JMenuItem("Instructions");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener(x-> InfoDialogTwoFields.show( "Instructions",  "Instructions for game you find on : ", "www.ultraboardgames.com/othello/game-rules.php"));
        menu.add(item);

        item = new JMenuItem("Authors");
        item.setMnemonic(KeyEvent.VK_N);
        item.addActionListener(x-> InfoDialogTwoFields.show( "Authors",  "David Kozák ... xkozak15@stud.fit.vutbr.cz",
                                                                "Peter Miklánek ... xmikla10@stud.fit.vutbr.cz"));
        menu.add(item);
    }
}