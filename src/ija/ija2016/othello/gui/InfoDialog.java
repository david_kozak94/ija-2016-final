/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >
 *
 *  Popis tridi InfoDialog :
 *  Pomocna trida pro vypis informaci
 */

package src.ija.ija2016.othello.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 *  Popis tridi InfoDialogTwoFields :
 *  Pomocna trida pro vypis informaci
 */
public class InfoDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField infoText;

    private InfoDialog(String title, String infoText) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        this.setTitle(title);
        this.infoText.setText(infoText);
        this.infoText.setColumns(infoText.length());

        buttonOK.addActionListener(x->dispose());

        this.pack();
        this.setLocationRelativeTo(null);
    }

    /**
     * Metoda pro vypis informaci
     * @param title nadpis
     * @param info prvni radek textu
     * @return dialog
     */
    public static InfoDialog show(String title, String info) {
        InfoDialog infoDialog = new InfoDialog(title, info);
        SwingUtilities.invokeLater(()-> infoDialog.setVisible(true));
        return infoDialog;
    }
}
