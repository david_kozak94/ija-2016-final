/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >              
 *  
 *  Popis rozhrani MainWindowCallback :
 *  Rozhrani specifikujici metody pro aktualizovani herniho okna pri zmenach stavu hry.
 */

package src.ija.ija2016.othello.gui;

import java.awt.*;
/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se konce hry, skore..
 */
public interface MainWindowCallback {

    /**
     * Konec hry
     * @param score game score
     */
    void endGame(Point score);
    void updateInfoField(String s);
    void updateScoreField(Point score);
    void updateTurnInfoField(boolean isWhite);
    void repaint();
}
