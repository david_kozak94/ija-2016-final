/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >               
 *  
 *  Popis tridi GenericDialog:
 *  Obecny dialog, ktery je schopen informovat uzivatele o libovolne situaci 
 *  s parametrizovatelnymi operacemi pro tlacitko OK a Cacncel
 */

package src.ija.ija2016.othello.gui;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 *  Obecny dialog, ktery je schopen informovat uzivatele o libovolne situaci
 *  s parametrizovatelnymi operacemi pro tlacitko OK a Cacncel
 */

public class GenericDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField msgField;

    private final Runnable okAction;
    private final Runnable cancelAction;

    public static GenericDialog show(String title, String msg){
        GenericDialog genericDialog = new GenericDialog(title,msg);
        SwingUtilities.invokeLater(()->genericDialog.setVisible(true));
        return genericDialog;
    }

    /**
     * Metoda reprezentujici Genericky dialog
     * @param title
     * @param msg
     */
    private GenericDialog(String title,String msg){
        this.setTitle(title);
        msgField.setText(msg);
        msgField.setColumns(msg.length());

        okAction = this::dispose;
        cancelAction = this::dispose;

        this.pack();
        this.setLocationRelativeTo(null);
    }

    public static GenericDialog show(String title,String msg,Runnable okAction,Runnable cancelAction) {
        GenericDialog genericDialog =  new GenericDialog(title,msg,okAction,cancelAction);
        SwingUtilities.invokeLater(()->genericDialog.setVisible(true));
        return  genericDialog;
    }

    /**
     * Metoda reprezentujici Genericky dialog
     * @param title
     * @param msg
     * @param okAction
     * @param cancelAction
     */
    private GenericDialog(String title,String msg,Runnable okAction,Runnable cancelAction) {
        this(title,msg,okAction,cancelAction,"OK","Cancel");
    }

    public static  GenericDialog show (String title,String msg,Runnable okAction,Runnable cancelAction,String okButtonText, String cancelButtonText) {
        GenericDialog genericDialog =  new GenericDialog(title,msg,okAction,cancelAction,okButtonText,cancelButtonText);
        SwingUtilities.invokeLater(()->genericDialog.setVisible(true));
        return  genericDialog;
    }

    /**
     * Metoda reprezentujici Genericky dialog
     * @param title
     * @param msg
     * @param okAction
     * @param cancelAction
     * @param okButtonText
     * @param cancelButtonText
     */
    private GenericDialog(String title,String msg,Runnable okAction,Runnable cancelAction,String okButtonText, String cancelButtonText) {
        this.okAction = okAction;
        this.cancelAction = cancelAction;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.setText(okButtonText);
        buttonCancel.setText(cancelButtonText);

        this.setTitle(title);
        msgField.setText(msg);

        buttonOK.addActionListener(x->onOK());
        buttonCancel.addActionListener(x->onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        this.setSize(300,100);
        this.setLocationRelativeTo(null);
    }

    /**
     * Metoda reprezentujici tlacitko OK
     */
    private void onOK() {
// add your code here
        okAction.run();
        dispose();
    }

    /**
     * Metoda reprezentujici tlacitko Cancel
     */
    private void onCancel() {
// add your code here if necessary
        cancelAction.run();
        dispose();
    }
}
