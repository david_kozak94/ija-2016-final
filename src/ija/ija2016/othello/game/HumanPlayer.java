/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >         	
 *	
 *  Popis tridi HumanPlayer :
 *	Trida, ktera reprezenentuje jednoho hrace.
 */

package src.ija.ija2016.othello.game;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida, ktera reprezenentuje jednoho hrace.
 */

public class HumanPlayer extends AbstractPlayer {
	
    /**
     * Konstruktor pro farbu hrace
     * @param isWhite true- bily, false- cerny
     */
    public HumanPlayer(boolean isWhite) {
        super(isWhite);
    }
}
