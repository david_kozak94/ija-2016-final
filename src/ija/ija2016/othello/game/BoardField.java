/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >             
 *  
 *  Popis tridi BoardField :
 *  Trida, ktera reprezentuje akticni pole hraci desky.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.board.AbstractField;
import src.ija.ija2016.othello.board.Disk;
import src.ija.ija2016.othello.board.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida, ktera reprezentuje akticni pole hraci desky.
 */

public class BoardField extends AbstractField implements IOthelloField {
    private volatile boolean isFrozen = false;

    /**
     * Konstruktor tridy Boardfield
     * @param row reprezentuje pocet radku
     * @param col reprezentuje pocet stlpcu
     */
    public BoardField(int row, int col) {
        super(row, col);
    }

    /**
     * Test, zda je mozne vlozit na pole kamen
     * @param disk kamen
     * @return true v pripade uspechu
     */
    @Override
    public boolean canPutDisk(Disk disk) {
        return this.disk == null;
    }

    /**
     * @param disk
     * @return
     */
    @Override
    public boolean putDisk(Disk disk) {
        if (this.disk == null) {
            this.disk = disk;
            return true;
        } else return false;
    }

    /**
     * Metoda reprezentuje pocet vymenenych kamenu
     * @param isWhite true- bily kamen, flase- cerny
     * @return pocet
     */
    @Override
    public int countChangedDisks(Boolean isWhite) {
        if (!this.isEmpty())
            return 0;

        List<Field> l = new ArrayList<>();

        for (Field.Direction dir : Field.Direction.values()) {
            Field f = this.nextField(dir);
            if (f != null) {
                f.checkMove(dir, isWhite, l);
            }
        }
        return l.size();
    }


    /**
     * Metoda pro kontrolu tahu
     * @param dirs - smer, jakym z daneho pole ma prohledavani jit dal
     * @param isWhite true-bily, false- cerny
     * @param stonesToChange kamen ktery se ma zmenit
     * @return true v pripade uspechu
     */
    @Override
    public boolean checkMove(Direction dirs, boolean isWhite, List<Field> stonesToChange) {

        // pokud na poli neni kamen, neni vkladani timto smerem mozne
        Disk d = getDisk();
        if (d != null) {
            // srovnani barvy kamenu
            if (d.isWhite() != isWhite) {
                stonesToChange.add(this);
                Field f = this.nextField(dirs);
                if (f != null)
                    return f.checkMove(dirs, isWhite, stonesToChange);
                else // pokud na dalsim poli jiz nic neni, nelze timto smerem kameny soupere menit
                {
                    stonesToChange.clear();
                    return false;
                }
            } else // pokud byl nalezen kamen shodne barvy, uspech lze poznat podle toho, zda mezi temito kameny lezi nejake kameny soupere
                return !stonesToChange.isEmpty(); // TODO mozna nebude fungovat pro dalsi smery , pokus uz z predchoziho
        } else {
            stonesToChange.clear();
            return false;
        }
    }

    /**
     * vraci kamen ktery je vlozen na pole
     * @return kamen
     */
    @Override
    public Disk getDisk() {
        // TODO potencial problem with canPutDisk directly on frozen field
        if (!isFrozen)
            return super.getDisk();
        else return null;
    }

    /**
     * metoda ktera vraci true v pripade že je kamen zamrzli
     * @return isFrozen vraci true v pripade uspechu
     */
    public boolean isFrozen() {
        return isFrozen;
    }

    /**
     * Metoda nastavy kamen ako zamrzly
     * @param frozen
     */
    public void setFrozen(boolean frozen) {
        isFrozen = frozen;
    }

    @Override
    public void preTest() {
    }

    @Override
    public void postTest() {
    }

    @Override
    public void removeDisk() {
        this.setFrozen(false);
        super.removeDisk();
    }

    @Override
    public Disk forceGetDisk() {
        return super.getDisk();
    }
}
