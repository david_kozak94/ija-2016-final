/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >           
 *  
 *  Popis tridi StoneFrozer :
 *  Vlakno, ktere implementuje mechanizmus "zamrzavani" hernich kamenu.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.LogProducer;
import src.ija.ija2016.othello.LogProducer.Loggable;
import src.ija.ija2016.othello.gui.MainWindowCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.util.stream.Collectors.toList;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Vlakno, ktere implementuje mechanizmus "zamrzavani" hernich kamenu
 */

public class StoneFrozer implements Serializable, Loggable,Runnable {
    private static final String TAG = "STONE_FROZER";

    private List<IOthelloField> fields;
    private int threadSleepingTime;
    private int stoneFreezingTime;
    private int stonesToFreeze;
    private int realSleepingTime;
    private transient MainWindowCallback callback;
    List<IOthelloField> frozenFields;

    /**
     * Konstruktor tridi StoneFrozer
     * @param callback
     * @param fields pole
     * @param freezerInfo info o zamrzani
     */
    public StoneFrozer(MainWindowCallback callback, List<IOthelloField> fields, FreezerInfo freezerInfo) {
        this.fields = fields;
        this.threadSleepingTime = freezerInfo.getThreadSleepingTime();
        this.stoneFreezingTime = freezerInfo.getStoneFreezingTime();
        this.stonesToFreeze = freezerInfo.getStonesToFreeze();
        this.callback = callback;
        frozenFields =  new ArrayList<>();
    }

    /**
     * Metoda reprezentujici beh zamrzani kamenu
     */
    @Override
    public void run() {
        Random rand = new Random();

        if(!frozenFields.isEmpty()){
            // if this is a "ressurection" of the thread, we have to unfroze the stones first
            try {
                Thread.sleep(realSleepingTime);
            } catch (InterruptedException e) {
                LogProducer.info(this,"Stone frozer sleep interrupted...");
                return;
            }
            frozenFields.stream().forEach(x->x.setFrozen(false));
        }

        while (true) {
            try {
                Thread.sleep(rand.nextInt(threadSleepingTime) * 1000);
            } catch (InterruptedException e) {
                LogProducer.info(this,"Stone frozer sleep interrupted...");
                return;
            }
            LogProducer.info(this,"StoneFrozer is waking up...");
            List<IOthelloField> activeFields = fields.stream().filter(x -> x.getDisk() != null).collect(toList());

            int len = stonesToFreeze <= activeFields.size() ? stonesToFreeze : activeFields.size();


            Collections.shuffle(activeFields);
            frozenFields = new ArrayList<>(activeFields.subList(0, len));
            frozenFields.stream().forEach(x -> x.setFrozen(true));
            callback.repaint();
            LogProducer.info(this,"Stone frozer has just frozen " + len + "disks");
            try {
                realSleepingTime = rand.nextInt(stoneFreezingTime) * 1000;
                Thread.sleep(realSleepingTime);
            } catch (InterruptedException e) {
                LogProducer.info(this,"Stone frozer sleep interrupted...");
                return;
            }
            frozenFields.stream().forEach(x -> x.setFrozen(false));
            LogProducer.info(this,len + " disks are free again");
            realSleepingTime = 0;
            callback.repaint();

        }
    }

    /**
     * Metoda nastavi zpetny volani
     * @param callback
     */
    public void setCallback(MainWindowCallback callback) {
        this.callback = callback;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    /**
     * Trida reprezentujici informace o zamrzani kamenu
     */
    public static class FreezerInfo {
        private final int threadSleepingTime;
        private final int stoneFreezingTime;
        private final int stonesToFreeze;

        /**
         * Metoda definujici informace o zamrzani
         * @param threadSleepingTime
         * @param stoneFreezingTime
         * @param stonesToFreeze
         */
        public FreezerInfo(int threadSleepingTime, int stoneFreezingTime, int stonesToFreeze) {
            this.threadSleepingTime = threadSleepingTime;
            this.stoneFreezingTime = stoneFreezingTime;
            this.stonesToFreeze = stonesToFreeze;
        }

        /**
         * Metoda vraci vlakno
         * @return threadSleepingTime
         */
        public int getThreadSleepingTime() {
            return threadSleepingTime;
        }

        /**
         * Metoda vraci kamen
         * @return stoneFreezingTime
         */
        public int getStoneFreezingTime() {
            return stoneFreezingTime;
        }

        /**
         * Metoda vraci kamen na zamrzani
         * @return stonesToFreeze
         */
        public int getStonesToFreeze() {
            return stonesToFreeze;
        }
    }
}
