/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >           
 *  
 *  Popis tridi AbstractPlayer :
 *  Abstraktni trida implementujici metody spolecne pro lidske hrace i pocitac.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.board.Board;
import src.ija.ija2016.othello.board.Disk;
import src.ija.ija2016.othello.board.Field;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import static java.util.stream.Collectors.toList;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Abstraktni trida implementujici metody spolecne pro lidske hrace i pocitac
 */

public abstract class AbstractPlayer implements Serializable{
    protected final boolean isWhite;
    private final Stack<Turn> oldTurns;

    /**
     * Konstruktor ktery nastavuje farbu hrace
     * @param isWhite true-bily , false-cerny
     */
    public AbstractPlayer(boolean isWhite){
        this.isWhite = isWhite;
        oldTurns = new Stack<>();
    }

    /**
     * Metoda pro vraceni farby hrace
     * @return isWhite true-bily , false-cerny
     */
    public boolean isWhite() {
        return isWhite;
    }

    /**
     * Metoda inicializuje prvni 4 kameny
     * @param board hraci deska
     */
    public void init(Board board) {
        int size = board.getSize();
        if (isWhite) {
            board.getField(size / 2, size / 2).putDisk(new Disk(true));
            board.getField(size / 2 + 1, size / 2 + 1).putDisk(new Disk(true));
        } else {
            board.getField(size / 2, size / 2 + 1).putDisk(new Disk(false));
            board.getField(size / 2 + 1, size / 2).putDisk(new Disk(false));
        }
    }

    @Override
    public String toString() {
        if (isWhite)
            return "white";
        else
            return "black";
    }

    /**
     * Metoda pro vraceni posledniho tahu
     */
    public void revertLastTurn() throws EmptyStackException {
        Turn turn = oldTurns.pop();
        turn.getChangesStones().stream().forEach(x->{
            x.forceGetDisk().turn();
            x.setFrozen(false);

        });
        turn.getNewStoneField().removeDisk();
    }

    /**
     * Metoda pro kontrolu, ci sa muze disk vlozit na dane policko
     * @param field pole
     */
    public boolean canPutDisk(Field field) {
        if (field == null || !field.isEmpty())
            return false;
        List<Field> l = new ArrayList<>();

        for (Field.Direction dir : Field.Direction.values()) {
            Field f = field.nextField(dir);
            if (f != null) {
                if (f.checkMove(dir, isWhite, l))
                    return true;
            }
        }
        return false;
    }
    
    /**
     * Metoda vklada kamen na desku
     * @param field pole hraci desky
     * @return true v pripad uspechu
     */
    public boolean putDisk(Field field) {
        List<Field> l = new ArrayList<>();
        List<Field> res = new ArrayList<>();


        for (Field.Direction dir : Field.Direction.values()) {
            Field f = field.nextField(dir);
            if (f != null) {
                f.checkMove(dir, isWhite, l);
            }
            res.addAll(l);
            l.clear();
        }
        if (res.isEmpty())
            return false;

        res.stream().forEach(x->x.getDisk().turn());
        oldTurns.push(new Turn(res.stream().map(x->(IOthelloField)x).collect(toList()),field));
        field.putDisk(new Disk(isWhite));
        return true;
    }
    
    /**
     * Trida Turn reprezentuje jeden herny tah
     */
    private final class Turn implements Serializable {
        private final Field newStoneField;
        private final List<IOthelloField> changesStones;
    
        /**
         * Konstruktor tridy Turn
         * @param changesStones list kamenu
         * @param f pole
         */
        public Turn(List<IOthelloField> changesStones,Field f) {
            this.changesStones = changesStones;
            newStoneField = f;
        }

        /**
         * Metoda getChangesStones vrací zmenene kameny
         * @return changesStones
         */
        public List<IOthelloField> getChangesStones() {
            return changesStones;
        }

        /**
         * Metoda vrací nové vložené pole
         * @return newStoneField
         */
        public Field getNewStoneField() {
            return newStoneField;
        }
    }
}
