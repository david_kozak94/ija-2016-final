/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >         	
 *	
 *  Popis rozhrani IOthelloField :
 *	Rozhrani specifikujici dalsi metody pole vazajici se ciste na hru Othello.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.board.Disk;
import src.ija.ija2016.othello.board.Field;

import java.io.Serializable;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se zamrzani kamenu
 */

public interface IOthelloField extends Field,Serializable {
    int countChangedDisks(Boolean isWhite);

    boolean isFrozen();
    void setFrozen(boolean frozen);
    Disk forceGetDisk();
}
