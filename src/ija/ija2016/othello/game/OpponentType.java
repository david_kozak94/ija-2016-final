/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >         		
 *	
 *  Popis enumu OpponentType :
 * 	Vyctovy typ reprezentujici lidskeho hrace a 2 varianty obtiznosti hrace pocitac.
 */

package src.ija.ija2016.othello.game;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Vyctovy typ reprezentujici lidskeho hrace a hrace pocitac
 */

public enum OpponentType {
    human,
    pc_easy,
    pc_hard
}
