/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >             
 *  
 *  Popis tridi ComputerPlayerEasy :
 *  Trida impementujici algoritmus pre hru s pocitacem s lehci obtiznosti
 */

package src.ija.ija2016.othello.game.ai;

import src.ija.ija2016.othello.board.Board;
import src.ija.ija2016.othello.board.Field;
import src.ija.ija2016.othello.game.AbstractPlayer;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida impementujici algoritmus pre hru s pocitacem s lehci obtiznosti
 */

public class ComputerPlayerEasy extends AbstractPlayer implements AI  {

    /**
     * Konstruktor pro farbu hrace
     * @param isWhite true- bily, false- cerny
     */
    public ComputerPlayerEasy(boolean isWhite){
        super(isWhite);
    }

    /**
     * Metoda reprezentujici dalsi tah
     * @param board hraci deska
     */
    @Override
    public void nextMove(Board board) {
        List<Field> f = board.getGameFields().stream().
                filter(super::canPutDisk)
                .collect(toList());

        super.putDisk(f
                .get(0));
    }
}
