/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >           
 *  
 *  Popis rozhrani Artifical intelligence :
 *  Funkcionalni rozhrani specifikujici signaturu metody AI pro vypocet tahu.
 */

package src.ija.ija2016.othello.game.ai;

import src.ija.ija2016.othello.board.AbstractField;
import src.ija.ija2016.othello.board.Board;
import src.ija.ija2016.othello.game.IOthelloField;
import src.ija.ija2016.othello.game.AbstractPlayer;
import src.ija.ija2016.othello.gui.InfoDialog;

import java.util.NoSuchElementException;

/**
 * @author David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version 1.00
 */

/**
 * Rozhrani obsahujici metody tykajici se algoritmu umnele inteligence
 */

public interface AI {
    void nextMove(Board board);

    static AbstractField computeHintMove(AbstractPlayer player, Board board) {

        IOthelloField f = board.getGameFields().stream().
                filter(player::canPutDisk).
                max((x, y) ->
                        Integer.compare(
                                x.countChangedDisks(player.isWhite()),
                                y.countChangedDisks(player.isWhite()))).
                get();
        return (AbstractField) f;

    }
}
