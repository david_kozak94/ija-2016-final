/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >         	
 *	
 *  Popis tridi ComputerPlayerHard :
 *  Trida impementujici algoritmus pre hru s pocitacem s tezsi obtiznosti
 */

package src.ija.ija2016.othello.game.ai;

import src.ija.ija2016.othello.board.Board;
import src.ija.ija2016.othello.game.IOthelloField;
import src.ija.ija2016.othello.game.AbstractPlayer;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 * Trida impementujici algoritmus pre hru s pocitacem s tezsi obtiznosti
 */

public class ComputerPlayerHard extends AbstractPlayer implements AI {
	
    /**
     * Konstruktor pro farbu hrace
     * @param isWhite true- bily, false- cerny
     */
    public ComputerPlayerHard(boolean isWhite) {
        super(isWhite);
    }

    /**
     * Metoda reprezentujici dalsi tah
     * @param board hraci deska
     */
    @Override
    public void nextMove(Board board) {
        IOthelloField f = board.getGameFields().stream().filter(super::canPutDisk).max((x,y)->
                Integer.compare(x.countChangedDisks(isWhite),y.countChangedDisks(isWhite))).get();

        super.putDisk(f);

    }
}
