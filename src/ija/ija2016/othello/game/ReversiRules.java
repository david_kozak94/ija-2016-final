/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >              
 *  
 *  Popis tridi ReversiRules :
 *  Trida, ktera reprezentuje aktivni pole hraci desky.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.board.BorderField;
import src.ija.ija2016.othello.board.Field;
import src.ija.ija2016.othello.board.Rules;

/**
 * @author      David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author      Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version     1.00
 */

/**
 *  Trida, ktera reprezentuje aktivni pole hraci desky
 */

public class ReversiRules implements Rules{
    private int size;

    /**
     * Konstruktor ktery nastavuje velkost esky
     * @param size velkost
     */
    public ReversiRules(int size) {
        this.size = size;
    }

    /**
     * Metoda pro urceni akticniho a neaktivniho pole
     * @param row pocet radku
     * @param col pocet stlpcu
     * @return BoardField aktivni pole desky, BorderField neakticni pole desky
     */
    @Override
    public Field createField(int row, int col) {
        if(row == 0 || col == 0 || row == size + 1 || col == size + 1)
            return new BorderField();
        else
            return new BoardField(row,col);
    }

    /**
     * Metoda pro ziskani velkosti pole
     * @return size velkost
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * metoda pro ziskani poctu kamenu, ktere muze jeden hrac vlozit na dane pole
     * @return pocet kamenu
     */
    @Override
    public int numberDisks() {
        return size*size  / 2;
    }
}
