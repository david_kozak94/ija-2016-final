/*
 *  Projekt:    Othello
 *  Predmet:    IJA - Seminar Java
 *  Datum:      23 Apr 2016
 *  Autori:     
 *              David Kozak     < xkozak15 @ stud.fit.vutbr.cz >
 *              Peter Miklanek  < xmikla10 @ stud.fit.vutbr.cz >             
 *  
 *  Popis tridi Game :
 *  Trida, ktera reprezentuje hru, spojuje dohromady desku a hrace.
 */

package src.ija.ija2016.othello.game;

import src.ija.ija2016.othello.board.Board;
import src.ija.ija2016.othello.game.ai.AI;
import src.ija.ija2016.othello.gui.InfoDialog;
import src.ija.ija2016.othello.gui.MainWindowCallback;

import java.awt.*;
import java.io.Serializable;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author David Kozak     <xkozak15 @ stud.fit.vutbr.cz>
 * @author Peter Miklanek  <xmikla10 @ stud.fit.vutbr.cz>
 * @version 1.00
 */

/**
 * Trida, ktera reprezentuje hru, spojuje dohromady desku a hrace.
 */

public class Game implements Serializable {
    private Board board;
    private transient Thread stoneFrozerThread;
    private StoneFrozer stoneFrozer;
    private transient MainWindowCallback callback;

    // hashmap to allow further expansion (other players)
    private HashMap<Boolean, AbstractPlayer> players;
    private boolean whiteTurn = false;

    public static final String DEFAULT_NAME = "New game";

    private String name;

    /**
     * Konstruktor pro objekt hraci desky
     *
     * @param board objekt hraci desky
     */
    public Game(Board board) {
        this.board = board;
        players = new HashMap<>();
    }

    /**
     * Metoda pre kontrolu tahu
     *
     * @param x suradnica na ose X
     * @param y suradnica na ose Y
     */
    public boolean checkMove(int x, int y) {
        return currentPlayer().canPutDisk(board.getField(x, y));
    }

    /**
     * Metoda computeScore
     *
     * @return
     */
    public Point computeScore() {
        List<IOthelloField> activeFields = getBoard().
                getGameFields().stream().filter(x -> x.forceGetDisk() != null).collect(toList());

        int size = activeFields.size();
        int whiteStonesCount = (int) activeFields.stream().filter((x) -> x.forceGetDisk().isWhite()).count();
        return new Point(whiteStonesCount, size - whiteStonesCount);
    }

    /**
     * Metoda pro realizaci tahu
     *
     * @param x suradnica na ose X
     * @param y suradnica na ose Y
     */
    public void move(int x, int y) {
        currentPlayer().putDisk(board.getField(x, y));
        // check if player can move...
        if (playerLost(currentPlayer().isWhite())) {
            callback.endGame(computeScore());
        } else if (board.isFull()) {
            callback.endGame(computeScore());
        }
        this.nextPlayer();
        callback.updateScoreField(computeScore());
        callback.updateInfoField("Last move: " + x + " " + y);
        callback.updateTurnInfoField(currentPlayer().isWhite());
        callback.repaint();
    }

    /**
     * Kontrola ci sa hrac nestratil ( co znamena ze nema ziadne disky na hraci deske)
     *
     * @param isWhite
     * @return boolean true=lost
     */
    private boolean playerLost(boolean isWhite) {
        return (board.getGameFields().stream().filter(x -> x.forceGetDisk() != null && x.forceGetDisk().isWhite() == isWhite).count() == 0)
                ||
                (board.getGameFields().stream().noneMatch(currentPlayer()::canPutDisk) && board.getGameFields().stream().noneMatch(IOthelloField::isFrozen));
    }

    /**
     * Metoda pro pridani hrace
     *
     * @param player objekt hrace
     * @return true v pripade uspechu
     */
    public boolean addPlayer(AbstractPlayer player) {
        AbstractPlayer p = players.get(player.isWhite());
        if (p != null)
            return false;
        else
            players.put(player.isWhite(), player);
        player.init(board);
        return true;
    }

    /**
     * Metoda pro pridani hrace
     *
     * @return true v pripade uspechu
     */
    public AbstractPlayer currentPlayer() {
        return players.get(whiteTurn);
    }

    /**
     * Metoda ktera vraci desku
     *
     * @return board deska
     */
    public Board getBoard() {
        return board;
    }

    /**
     * Metoda pro urceni dalsiho hrace, ktery je na tahu
     *
     * @return currentPlayer aktualni hrac, ktery je na tahu
     */
    public AbstractPlayer nextPlayer() {
        whiteTurn = !whiteTurn;
        AbstractPlayer newPlayer = players.get(whiteTurn);

        // pokud je druhy hrac pocitac, hned zahrej tah a zmen hrace zpet
        if (newPlayer instanceof AI) {
            ((AI) newPlayer).nextMove(board);
            whiteTurn = !whiteTurn;
        }
        return currentPlayer();
    }

    /**
     * Metoda pro vrateni posledniho tahu
     *
     * @return true v pripade uspechu
     */
    public boolean revertLastTurn() {
        whiteTurn = !whiteTurn;
        try {
            currentPlayer().revertLastTurn();
            callback.repaint();
            return true;
        } catch (EmptyStackException e) {
            whiteTurn = !whiteTurn; // revert back to the current player
            InfoDialog.show("Info","There are no moves left to revert, you are at the beginning of the game");
            return false;
        }
    }

    /**
     * Metoda pro ziskani jmena
     *
     * @return name meo
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda pro nastaveni jmena
     *
     * @param name jmeno
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pro zamrzani kamene
     *
     * @return stoneFrozer
     */
    public Thread getStoneFrozerThread() {
        return stoneFrozerThread;
    }

    public StoneFrozer getStoneFrozer() {
        return stoneFrozer;
    }

    /**
     * Metoda pro nastaveni, ktere kameny maji "zamrznout"
     *
     * @param stoneFrozer
     */
    public void setStoneFrozer(StoneFrozer stoneFrozer) {
        this.stoneFrozer = stoneFrozer;
    }

    /**
     * Metoda pro zpetne volani
     *
     * @param callback
     */
    public void setCallback(MainWindowCallback callback) {
        this.callback = callback;
    }

    public void restartStoneFrozerThread() {
        if (stoneFrozerThread != null) {
            stoneFrozerThread.interrupt();
            try {
                stoneFrozerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        stoneFrozerThread = new Thread(stoneFrozer);
        stoneFrozerThread.start();
    }
}
