DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

wget http://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar 	-O ${DIR}/junit-4.12.jar
wget http://central.maven.org/maven2/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar 	-O ${DIR}/hamcrest-core-1.3.jar
wget https://sourceforge.net/projects/ija4/files/forms_rt.jar/download			-O ${DIR}/forms_rt.jar
wget https://sourceforge.net/projects/ija4/files/javac2.jar/download 			-O ${DIR}/javac2.jar
wget https://sourceforge.net/projects/ija4/files/jdom.jar/download 			-O ${DIR}/jdom.jar
wget https://sourceforge.net/projects/ija4/files/jgoodies-forms.jar/download 		-O ${DIR}/jgoodies-forms.jar
wget https://sourceforge.net/projects/ija4/files/annotations.jar/download 		-O ${DIR}/annotations.jar
wget https://sourceforge.net/projects/ija4/files/asm-all.jar/download 			-O ${DIR}/asm-all.jar
